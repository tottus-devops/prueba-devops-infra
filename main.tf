terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.61.0"
    }
  }
  backend "azurerm" {}
}

provider "azurerm" {
  skip_provider_registration = true
  features {}
}

resource "azurerm_container_registry" "container_registry" {
  name                = "crttpoc101"
  location            = "East US"
  resource_group_name = "RG_EUS_TT_DEV_01"
  admin_enabled       = "true"
  sku                 = "Standard"
}

resource "azurerm_kubernetes_cluster" "example" {
  name                = "k8seusttpoc101"
  location            = "East US"
  resource_group_name = "RG_EUS_TT_DEV_01"
  dns_prefix          = "k8seusttpoc101"
  default_node_pool {
    name       = "default"
    node_count = 2
    vm_size    = "Standard_D2_v2"
  }
  identity {
    type = "SystemAssigned"
  }
  tags = {
    Environment = "Production"
  }
}
