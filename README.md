## Etapa 4 Construcción de CI/CD

  2. Automatiza la etapa 3, logrando crear infraestructura de forma segura y automatizada.
  
```bash
Running with gitlab-runner 14.3.0-rc1 (ed15bfbf)
on docker-auto-scale fa6cab46
Preparing the "docker+machine" executor 00:09
Using Docker executor with image hashicorp/terraform:latest ...
Pulling docker image hashicorp/terraform:latest ...
Using docker image sha256:9a5f4cb4589a033182e63c23cc61e6828729e645155e827d28367b6473e44126 for hashicorp/terraform:latest with digest hashicorp/terraform@sha256:122487e6bf0b09a88ffc5c9044de6ceccfa1ecd145cccd9d9c4c06930d4bbefe ...
Preparing environment 00:01
Running on runner-fa6cab46-project-29845156-concurrent-0 via runner-fa6cab46-srm-1632404105-d5320156...
Getting source from Git repository 00:02
$ eval "$CI_PRE_CLONE_SCRIPT"
Fetching changes with git depth set to 50...
hint: Using 'master' as the name for the initial branch. This default branch name
hint: is subject to change. To configure the initial branch name to use in all
hint: of your new repositories, which will suppress this warning, call:
hint: 
hint: 	git config --global init.defaultBranch <name>
hint: 
hint: Names commonly chosen instead of 'master' are 'main', 'trunk' and
hint: 'development'. The just-created branch can be renamed via this command:
hint: 
hint: 	git branch -m <name>
Initialized empty Git repository in /builds/tottus-devops/iac/.git/
Created fresh repository.
Checking out 0309d01b as main...
Skipping Git submodules setup
Executing "step_script" stage of the job script 04:29
Using docker image sha256:9a5f4cb4589a033182e63c23cc61e6828729e645155e827d28367b6473e44126 for hashicorp/terraform:latest with digest hashicorp/terraform@sha256:122487e6bf0b09a88ffc5c9044de6ceccfa1ecd145cccd9d9c4c06930d4bbefe ...
$ terraform get -update=true
$ terraform init -backend-config=backend.conf -backend=true -force-copy -get=true -input=false -upgrade
Initializing the backend...
Successfully configured the backend "azurerm"! Terraform will automatically
use this backend unless the backend configuration changes.
Initializing provider plugins...
- Finding hashicorp/azurerm versions matching "2.61.0"...
- Installing hashicorp/azurerm v2.61.0...
- Installed hashicorp/azurerm v2.61.0 (signed by HashiCorp)
Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.
Terraform has been successfully initialized!
You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.
If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
$ terraform plan -out=out.tfplan
Terraform used the selected providers to generate the following execution
plan. Resource actions are indicated with the following symbols:
+ create
Terraform will perform the following actions:
# azurerm_container_registry.container_registry will be created
+ resource "azurerm_container_registry" "container_registry" {
    + admin_enabled                 = true
    + admin_password                = (sensitive value)
    + admin_username                = (known after apply)
    + encryption                    = (known after apply)
    + georeplication_locations      = (known after apply)
    + georeplications               = (known after apply)
    + id                            = (known after apply)
    + location                      = "eastus"
    + login_server                  = (known after apply)
    + name                          = "crttpoc101"
    + network_rule_set              = (known after apply)
    + public_network_access_enabled = true
    + resource_group_name           = "RG_EUS_TT_DEV_01"
    + retention_policy              = (known after apply)
    + sku                           = "Standard"
    + trust_policy                  = (known after apply)
    + identity {
        + identity_ids = (known after apply)
        + principal_id = (known after apply)
        + type         = (known after apply)
        }
    }
# azurerm_kubernetes_cluster.example will be created
+ resource "azurerm_kubernetes_cluster" "example" {
    + dns_prefix              = "k8seusttpoc101"
    + fqdn                    = (known after apply)
    + id                      = (known after apply)
    + kube_admin_config       = (known after apply)
    + kube_admin_config_raw   = (sensitive value)
    + kube_config             = (known after apply)
    + kube_config_raw         = (sensitive value)
    + kubelet_identity        = (known after apply)
    + kubernetes_version      = (known after apply)
    + location                = "eastus"
    + name                    = "k8seusttpoc101"
    + node_resource_group     = (known after apply)
    + private_cluster_enabled = (known after apply)
    + private_dns_zone_id     = (known after apply)
    + private_fqdn            = (known after apply)
    + private_link_enabled    = (known after apply)
    + resource_group_name     = "RG_EUS_TT_DEV_01"
    + sku_tier                = "Free"
    + tags                    = {
        + "Environment" = "Production"
        }
    + addon_profile {
        + aci_connector_linux {
            + enabled     = (known after apply)
            + subnet_name = (known after apply)
            }
        + azure_policy {
            + enabled = (known after apply)
            }
        + http_application_routing {
            + enabled                            = (known after apply)
            + http_application_routing_zone_name = (known after apply)
            }
        + ingress_application_gateway {
            + effective_gateway_id                 = (known after apply)
            + enabled                              = (known after apply)
            + gateway_id                           = (known after apply)
            + ingress_application_gateway_identity = (known after apply)
            + subnet_cidr                          = (known after apply)
            + subnet_id                            = (known after apply)
            }
        + kube_dashboard {
            + enabled = (known after apply)
            }
        + oms_agent {
            + enabled                    = (known after apply)
            + log_analytics_workspace_id = (known after apply)
            + oms_agent_identity         = (known after apply)
            }
        }
    + auto_scaler_profile {
        + balance_similar_node_groups      = (known after apply)
        + empty_bulk_delete_max            = (known after apply)
        + expander                         = (known after apply)
        + max_graceful_termination_sec     = (known after apply)
        + max_node_provisioning_time       = (known after apply)
        + max_unready_nodes                = (known after apply)
        + max_unready_percentage           = (known after apply)
        + new_pod_scale_up_delay           = (known after apply)
        + scale_down_delay_after_add       = (known after apply)
        + scale_down_delay_after_delete    = (known after apply)
        + scale_down_delay_after_failure   = (known after apply)
        + scale_down_unneeded              = (known after apply)
        + scale_down_unready               = (known after apply)
        + scale_down_utilization_threshold = (known after apply)
        + scan_interval                    = (known after apply)
        + skip_nodes_with_local_storage    = (known after apply)
        + skip_nodes_with_system_pods      = (known after apply)
        }
    + default_node_pool {
        + max_pods             = (known after apply)
        + name                 = "default"
        + node_count           = 2
        + orchestrator_version = (known after apply)
        + os_disk_size_gb      = (known after apply)
        + os_disk_type         = "Managed"
        + type                 = "VirtualMachineScaleSets"
        + vm_size              = "Standard_D2_v2"
        }
    + identity {
        + principal_id = (known after apply)
        + tenant_id    = (known after apply)
        + type         = "SystemAssigned"
        }
    + network_profile {
        + dns_service_ip     = (known after apply)
        + docker_bridge_cidr = (known after apply)
        + load_balancer_sku  = (known after apply)
        + network_mode       = (known after apply)
        + network_plugin     = (known after apply)
        + network_policy     = (known after apply)
        + outbound_type      = (known after apply)
        + pod_cidr           = (known after apply)
        + service_cidr       = (known after apply)
        + load_balancer_profile {
            + effective_outbound_ips    = (known after apply)
            + idle_timeout_in_minutes   = (known after apply)
            + managed_outbound_ip_count = (known after apply)
            + outbound_ip_address_ids   = (known after apply)
            + outbound_ip_prefix_ids    = (known after apply)
            + outbound_ports_allocated  = (known after apply)
            }
        }
    + role_based_access_control {
        + enabled = (known after apply)
        + azure_active_directory {
            + admin_group_object_ids = (known after apply)
            + azure_rbac_enabled     = (known after apply)
            + client_app_id          = (known after apply)
            + managed                = (known after apply)
            + server_app_id          = (known after apply)
            + server_app_secret      = (sensitive value)
            + tenant_id              = (known after apply)
            }
        }
    + windows_profile {
        + admin_password = (sensitive value)
        + admin_username = (known after apply)
        }
    }
Plan: 2 to add, 0 to change, 0 to destroy.
─────────────────────────────────────────────────────────────────────────────
Saved the plan to: out.tfplan
To perform exactly these actions, run the following command to apply:
    terraform apply "out.tfplan"
$ terraform apply --auto-approve out.tfplan
azurerm_kubernetes_cluster.example: Creating...
azurerm_container_registry.container_registry: Creating...
azurerm_container_registry.container_registry: Creation complete after 6s [id=/subscriptions/e5196b22-73e4-4afa-86c0-313544fd5911/resourceGroups/RG_EUS_TT_DEV_01/providers/Microsoft.ContainerRegistry/registries/crttpoc101]
azurerm_kubernetes_cluster.example: Still creating... [10s elapsed]
azurerm_kubernetes_cluster.example: Still creating... [20s elapsed]
azurerm_kubernetes_cluster.example: Still creating... [30s elapsed]
azurerm_kubernetes_cluster.example: Still creating... [40s elapsed]
azurerm_kubernetes_cluster.example: Still creating... [50s elapsed]
azurerm_kubernetes_cluster.example: Still creating... [1m0s elapsed]
azurerm_kubernetes_cluster.example: Still creating... [1m10s elapsed]
azurerm_kubernetes_cluster.example: Still creating... [1m20s elapsed]
azurerm_kubernetes_cluster.example: Still creating... [1m30s elapsed]
azurerm_kubernetes_cluster.example: Still creating... [1m40s elapsed]
azurerm_kubernetes_cluster.example: Still creating... [1m50s elapsed]
azurerm_kubernetes_cluster.example: Still creating... [2m0s elapsed]
azurerm_kubernetes_cluster.example: Still creating... [2m10s elapsed]
azurerm_kubernetes_cluster.example: Still creating... [2m20s elapsed]
azurerm_kubernetes_cluster.example: Still creating... [2m30s elapsed]
azurerm_kubernetes_cluster.example: Still creating... [2m40s elapsed]
azurerm_kubernetes_cluster.example: Still creating... [2m50s elapsed]
azurerm_kubernetes_cluster.example: Still creating... [3m0s elapsed]
azurerm_kubernetes_cluster.example: Still creating... [3m10s elapsed]
azurerm_kubernetes_cluster.example: Still creating... [3m20s elapsed]
azurerm_kubernetes_cluster.example: Still creating... [3m30s elapsed]
azurerm_kubernetes_cluster.example: Still creating... [3m40s elapsed]
azurerm_kubernetes_cluster.example: Still creating... [3m50s elapsed]
azurerm_kubernetes_cluster.example: Still creating... [4m0s elapsed]
azurerm_kubernetes_cluster.example: Creation complete after 4m9s [id=/subscriptions/e5196b22-73e4-4afa-86c0-313544fd5911/resourcegroups/RG_EUS_TT_DEV_01/providers/Microsoft.ContainerService/managedClusters/k8seusttpoc101]
Apply complete! Resources: 2 added, 0 changed, 0 destroyed.
Cleaning up project directory and file based variables 00:00
Job succeeded
```